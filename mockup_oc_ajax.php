<?php 
    // MOCKUP OC_AJAX - DO NOT USE IN PRODUCTION

    if( !isset( $_SESSION ) ) {
        session_start();
    }

    $method     = isset($_POST["method"])    ? $_POST["method"]    : "none";
    $cpf        = isset($_POST["cpf"])       ? $_POST["cpf"]       : "none";
    $password   = isset($_POST["password"])  ? $_POST["password"]  : "none";
    $zipcode    = isset($_POST["zipcode"])   ? $_POST["zipcode"]   : "none";
    $street     = isset($_POST["street"])    ? $_POST["street"]    : "none";
    $district   = isset($_POST["district"])  ? $_POST["district"]  : "none";    
    $city       = isset($_POST["city"])      ? $_POST["city"]      : "none";
    $state      = isset($_POST["state"])     ? $_POST["state"]     : "none";
    
    if (isset($cpf) && ($cpf != "none")) {
        $_SESSION['user_cpf']               = $cpf;
        $_SESSION["oc_customer_id"]         = 3109;
        $_SESSION["oc_customer_group_id"]   = 1;
        $_SESSION["oc_first_name"]          = "Albeiro";
        $_SESSION["oc_last_name"]           = "Valencia";
    }

    if (isset($method)) { 
        switch (strtolower($method)) { 

            case "user_password":
                if (!isset($password)) { 
                    respond([], "Password not found", 401);
                }    

                if ( $password == "01234" )
                    respond( [ "success" => true ] );
                else
                    respond( [ "success" => false ] );
    
                break;

            case "get_customer_cpf":
                if (!isset($cpf)) { 
                    respond([], "CPF not found", 401);
                }
    
                if ( $cpf == "12345678" )
                    respond([
                          "success" => true,
                          "email"   => "albeiroval@gmail.com",
                          "name"    => "Albeiro Valencia"
                      ]);
                else
                    respond([
                        "success" => false,
                        "email"   => "error mail",
                        "name"    => "error name"
                      ]);
    
                break;

            case "list_user_addresses":

                if ( !isset( $_SESSION['address'] ) )   
                   $_SESSION['address'] = InitializeListAddress();
                
                respond([
                    "success" => true,
                    "address" => $_SESSION['address']
                ]);    

                break;

            case "new_user_addresses":    

                /*
                if ( !isset( $_SESSION['address'] ) ) {
                    $_SESSION['address'] = InitializeListAddress();    
                } 
                */   

                $_SESSION['address'] = InitializeListAddress();    

                $newAddress  = "";
                $newAddress .= $street . ", " . $district . ", <br> ";
                $newAddress .= $city   . "/"  . $state    . ", " . $zipcode;

                // console_php($newAddress);

                $address   = $_SESSION['address'];
                $address[] = $newAddress;

                $_SESSION['address'] = $address;    

                respond([
                    "success" => true,
                    "address" => $_SESSION['address']
                ]); 

                break;

            case "get_user_info":
                // Need to use suffixes
                $user_cpf = checkSession("user_cpf");
                $suffix_app = checkSession("suffix_app");
                $suffix_oc = checkSession("suffix_oc");

                respond([
                    "first_name"        => "Nombre",
                    "last_name"         => "Apellido",
                    "email"             => "E-mail",
                    "phone"             => "Phone",
                    "address_1"         => "Avenida 1234, 99",
                    "address_2"         => "Centro",
                    "city"              => "Cidade",
                    "state"             => "UF",
                    "zipcode"           => "12345"
                ]);
                break;

            case "get_customer_email":
                  $cpf = filter_input(INPUT_GET, "cpf");
                  if (!isset($cpf)) { 
                      respond([], "CPF not found", 401);
                  }
  
                  // Need to use suffixes
                  $suffix_app = checkSession("suffix_app");
                  $suffix_oc = checkSession("suffix_oc");
  
                  respond([
                      "email" => "email@email.com"
                  ]);
  
                  break;

            case "list_delivery_times":
                $store_id = filter_input(INPUT_GET, "store_id");
                if (!isset($store_id)) { 
                    respond([], "Store not found", 401);
                }
                if (!is_numeric($store_id)) { 
                    respond([], "Invalid Store", 401);
                }

                // Need to use suffixes
                $suffix_app = checkSession("suffix_app");
                $suffix_oc = checkSession("suffix_oc");

                // Returns a list of the next 2 days or 20 time slots
                echo "<option value='0112' selected>Lunes, 12:00 a 13:00</option>";
                echo "<option value='0113'>Lunes, 13:00 a 14:00</option>";
                echo "<option value='0114'>Lunes, 14:00 a 15:00</option>";
                echo "<option value='0115'>Lunes, 15:00 a 16:00</option>";
                echo "<option value='0116'>Lunes, 16:00 a 17:00</option>";
                echo "<option value='0212'>Martes, 12:00 a 13:00</option>";
                echo "<option value='0213'>Martes, 13:00 a 14:00</option>";
                die("");
               
                break;
            
            case "list_stores":
                $suffix_app = $_SESSION["suffix_app"];
                if (!isset($suffix_app)) { 
                    respond([], "Suffix not found", 500);
                }
                
                echo "<option value='1' data-content='Tienda Uno <BR>Avenida 1, 1234 <BR>Centro - Ciudad/UF' selected>Tienda Uno <BR>Avenida 1, 1234 <BR>Centro - Ciudad/UF</option>";
                echo "<option value='2' data-content='Tienda Dos <BR>Avenida 9, 9876 <BR>Centro - Ciudad2/UF'>Tienda Dos <BR>Avenida 9, 9876 <BR>Centro - Ciudad2/UF</option>";
                die("");

                break;

            case "fetch_zipcode":
            case "fetch_zip_code":
                if (!isset($zipcode)) { 
                    respond([], "Zip code parameter not found", 401);
                }

                if ($zipcode == "01111111" || $zipcode == "02222222" || $zipcode == "03333333") 
                    respond([
                        "success"   => true,
                        "zipcode"   => $zipcode,
                        "street"    => "Calle Uno",
                        "district"  => "Barrio",
                        "city"      => "Francisco Beltrão",
                        "state"     => "PR"
                    ]);
                else
                    respond([ "success"=> false ]);

                break;

			case "choose_pickup_store":
				// Need to use suffixes
				$oc_customer_id = checkSession("oc_customer_id");
				$oc_customer_group_id = checkSession("oc_customer_group_id");
				$suffix_app = checkSession("suffix_app");
				$suffix_oc = checkSession("suffix_oc");

				$id = filter_input(INPUT_POST, "id");
				if (!isset($id)) { 
					respond([], "Id parameter not found", 401);
				}

				$_SESSION["delivery_id"] = $id;

				respond([], "", 204);
				break;


			default:
				respond([], "Method not allowed", 405);
				break;
            
        } 
    } else {
		respond([], "Method not found", 401);
	}

    function respond($data = [], $message = "", $http_code = 200) {
        // OK with no content
        if ($http_code == 204) {
            http_response_code(204);
            die();
        }
        
        $answer = [];
        $answer["message"] = $message; 
        $answer["data"] = $data;
        http_response_code($http_code);
        die(json_encode($answer));
    }

    function checkSession($suffix) {
        $answer = $_SESSION[$suffix];
        if (!isset($answer)) { 
            respond([], "Session data $suffix not found", 500);
        }
        return $answer;
    }

    function getDOWname($dow) {
        $names = ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"];
        if (!isset($dow)) return false;
        if (!is_numeric($dow)) return false;
        if ($dow < 0) return false;
        if ($dow > 6) return false;
        return $names[$dow];
    }

    function InitializeListAddress() {

        $address   = array();
        $address[] = array("Avenida 1234, 99 Centro, <br> Cidade/UF, 12345");     
        $address[] = array("Calle 9876, 11 Barrio, <br> Ciudad/UF, 98765");     
        $address[] = array("Rua Abilio da Rocha, Bairro Luther King, <br> Francisco Beltrão/PR, 3309");     
        
        return $address;
    }

    // Debuger 
    function console_php( $data ) {
	    ob_start();
	    $output  = "<script>console.log( 'PHP debugger: ";
	    $output .= json_encode( print_r( $data, true ) );
	    $output .= "' );</script>";
	    echo $output;
    }

?>
<?php
class ControllerAccountAddressItalo extends Controller {
	private $error = array();

	public function index() {

		if (!function_exists('console_php')) 
			include('console_php.php');

		$button = (isset($_POST['newbtn'])) ? $_POST['newbtn'] 	: "YES";

		if ($button == "YES")
				$newbtn = true;
		else		
				$newbtn = false;

		$this->load->language('account/login');
		
		$this->document->addScript('catalog/view/javascript/tools.js');

		$this->document->addStyle('https://cdn.materialdesignicons.com/2.1.19/css/materialdesignicons.min.css');

		/*
		$this->document->addScript("catalog/view/javascript/bootstrap-select/js/bootstrap-select.min.js");
		$this->document->addStyle("catalog/view/javascript/bootstrap-select/css/bootstrap-select.min.css");
		*/
		
    console_php('addressitalo.php load success ' . 'newbtn ' . $newbtn);                    
    
    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_login'),
			'href' => $this->url->link('account/login', '', true)
		);

    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');
    
		$data['title']  	= 'Por favor, escolha o endereço para a entrega';
		$data['textnew'] 	= 'Novo endereço';
		$data['textlist'] = 'Utilizar endereço selecionado';
		$data['newbtn']		= $newbtn;
		
    $this->response->setOutput($this->load->view('account/addressitalo', $data));

  }

}
<?php
class ControllerAccountLoginitalo extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('account/login');
		
		$this->document->addScript('catalog/view/javascript/tools.js');

    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_login'),
			'href' => $this->url->link('account/login', '', true)
		);

    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');
    
    $data['title'] = 'Entrar / Registrar';
    $data['label'] = 'Digite seu CPF para acessar nosso site, 
											ter acesso a ofertas exclusivas e começar suas compras';
		
    $this->response->setOutput($this->load->view('account/loginitalo', $data));
  }

}
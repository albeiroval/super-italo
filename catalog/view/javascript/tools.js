/**
* Funcion para hacer un submit y pasar los parametros via post.
* Ejemplo :
* 	url_redirect( { url: 'my_file.php', method: 'post', data: { LAST: cLast, NUM: 1234 } } );
*/
function url_redirect( options ) {
    var $form = $( '<form />' );

	$form.attr( 'action', options.url );
	$form.attr( 'method', options.method );

	for ( var data in options.data )
	{
		$form.append( '<input type="hidden" name="' + data + '" value="' + options.data[ data ] + '" />' );
	}

	 $( 'body' ).append( $form );
	 $form.submit();
}

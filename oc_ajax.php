<?php 
    session_start();
    // Session
    $suffix_app = "italo";
    $suffix_oc = "dlv_italo";
    $_SESSION["suffix_app"] = $suffix_app;
    $_SESSION["suffix_oc"] = $suffix_oc;

    if ($_GET["debug2"] == "19880210") { 
        ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL); 
        $_SESSION["user_cpf"] = "36867422845";
        $_SESSION["oc_customer_id"] = 3109;
        $_SESSION["oc_customer_group_id"] = 1;
        $_SESSION["oc_first_name"] = "Marcus";
        $_SESSION["oc_last_name"] = "Araujo";
    }

    include("/home/u122747008/.big2be/classes/autoload.php");
    
    $method = filter_input(INPUT_GET, "method");
    if (isset($method)) { 
        switch (strtolower($method)) { 
            case "list_user_addresses":
                // Need to use suffixes
                $user_cpf = checkSession("user_cpf");
                $oc_customer_id = checkSession("oc_customer_id");
                $oc_customer_group_id = checkSession("oc_customer_group_id");

                $customer = new Customer($suffix_app, $suffix_oc);
                $info = $customer->getInfo($user_cpf, $oc_customer_id, $oc_customer_group_id);

                if ($info === false) {
                    die("");
                } else {
                    if (count($info->addresses_oc) > 0) {
                        $answer = "";
                        $selected = "selected";
                        foreach ($info->addresses_oc as $address) {
                            $content = "{$address->address_1}, <BR>{$address->address_2}, {$address->zone}, {$address->zipcode}";
                            $answer .= "<option data-content='$content' value='{$address->id}' $selected>$content</option>";
                            $selected = "";
                        }
                        die($answer);
                    } else { 
                        die("");
                    }
                }

                break;

            case "get_user_info":
                // Need to use suffixes
                $user_cpf = checkSession("user_cpf");
                $oc_customer_id = checkSession("oc_customer_id");
                $oc_customer_group_id = checkSession("oc_customer_group_id");

                $customer = new Customer($suffix_app, $suffix_oc);
                $info = $customer->getInfo($user_cpf, $oc_customer_id, $oc_customer_group_id);
  
                if ($info === false) {
                    respond([], "Customer not found", 404);
                } else { 
                    $has_address = $info->hasAddress();
                    respond([
                        "first_name"        => $info->first_name,
                        "last_name"         => $info->last_name,
                        "email"             => $info->email,
                        "phone"             => $info->phone,
                        "address_1"         => $has_address ? $info->address_app->street . " " . $info->address_app->number . " " . $info->address_app->complement : "",
                        "address_2"         => $has_address ? $info->address_app->district : "",
                        "city"              => $has_address ? $info->address_app->city : "",
                        "state"             => $has_address ? $info->address_app->state : "",
                        "zipcode"           => $has_address ? $info->address_app->zipcode : ""
                    ]);
                }
                break;

            case "get_customer_email":
                $cpf = filter_input(INPUT_GET, "cpf");
                if (!isset($cpf)) { 
                    respond([], "CPF not found", 401);
                }

                $customer = new Customer($suffix_app, $suffix_oc);
                $info = $customer->getInfo($cpf);

                if ($info === false) {
                    respond([], "Customer not found", 404);
                } else { 
                    respond([
                        "email" => $info->email
                    ]);
                }

                break;
            
            case "list_delivery_times":
                $store_id = filter_input(INPUT_GET, "store_id");
                if (!isset($store_id)) { 
                    respond([], "Store not found", 401);
                }
                if (!is_numeric($store_id)) { 
                    respond([], "Invalid Store", 401);
                }

                // Returns a list of the next 2 days or 20 time slots
                $stores = new Stores($suffix_app);
                $minutes = $stores->getMinDeliveryTimeMinutes();
                if ($minutes !== false) {
                    $list = $stores->listDeliveryTimes($store_id);
                    if ($list !== false) {
                        $original_date = getDateTZ("c");
                        $current_hour = getDateTZ("H");
                        $search_dow = getDateTZ("w");

                        $search_hour = $current_hour + ceil($minutes/60);
                        if ($search_hour > 24) {
                            $search_hour -= 24;
                            $search_dow += 1;
                            if ($search_dow >= 7) {
                                $search_dow -= 7;
                            }
                        }

                        $answer = "";
                        $available_times_hours_offset = [];
                        $hours_offset = 0;
                        $last_hour = $search_hour;
                        $total_days = 1;
                        while (count($available_times_hours_offset) < 20 && $total_days <= 2) {
                            foreach($list[$search_dow] as $hour) {
                                if ($hour >= $last_hour) {
                                    $hours_offset += $hour - $last_hour;
                                    $last_hour = $hour;
                                    $available_times_hours_offset[] = $hours_offset;
                                    
                                }
                            }
                            $search_dow++;
                            $search_hour = 8;
                            $hours_offset += 8 + (24 - $last_hour);
                            $last_hour = 8;
                            $search_dow = $search_dow == 6 ? 0 : $search_dow;
                            $total_days++;
                        }

                        $selected = "selected";
                        foreach ($available_times_hours_offset as $offset) {
                            $date_from = new DateTime($original_date);
                            $date_to = new DateTime($original_date);
                            $date_from->setTime($date_from->format("H"),0,0)->add(new DateInterval("P0DT" . $offset . "H"));
                            $date_to->setTime($date_to->format("H"),0,0)->add(new DateInterval("P0DT" . ($offset + 1) . "H"));

                            $dow_name = getDOWname($date_to->format('w'));
                            $value = $date_from->format('Y-m-d H:i:s');
                            $time_from = $date_from->format('d/M, H');
                            $time_to = $date_to->format('H');
                            $formatted_date = "$dow_name, $time_from:00 às $time_to:00";
                            $answer .= "<option value='$value' $selected>$formatted_date</option>";
                            $selected = "";
                        }
                        die($answer);

                    } else { 
                        respond([], "No delivery times found", 404);
                    }
                } else {
                    respond([], "Undefined store delivery times", 500);
                }

                break;
            
            case "list_stores":
                $stores = new Stores($suffix_app);
                $list = $stores->listStores();

                if ($list === false) {
                    respond([], "Invalid suffix or server error", 401);
                } else {
                    $answer = "";
                    $selected = "selected";
                    foreach ($list as $store) {
                        $content = "{$store->name} <BR>{$store->address_1} <BR>{$store->address_2}";
                        $answer .= "<option data-content='$content' value='{$store->unit}' $selected>$content</option>";
                        $selected = "";
                    }
                    die($answer);
                }

                break;

            case "fetch_zipcode":
            case "fetch_zip_code":
                $zipcode = filter_input(INPUT_GET, "zipcode");
                if (!isset($zipcode)) { 
                    respond([], "Zip code parameter not found", 401);
                }
                
                $data = Address::fetchZipcode($zipcode);
                if ($data === false) {
                    respond([], "Zip code not found", 200);
                }

                respond([
                    "zipcode"   => $data->zipcode,
                    "street"    => $data->street,
                    "district"  => $data->district,
                    "city"      => $data->city,
                    "state"     => $data->state
                ]);

                break;

            case "choose_pickup_store":
                // Need to use suffixes
                $oc_customer_id = checkSession("oc_customer_id");
                $oc_customer_group_id = checkSession("oc_customer_group_id");

                $store_id = filter_input(INPUT_POST, "id");
                if (!isset($store_id)) { 
                    respond([], "Parameter 'id' not found", 401);
                }

                $customer = new Customer($suffix_app, $suffix_oc);
                if ($customer->chooseStore($oc_customer_id, $oc_customer_group_id, $store_id)) {
                    respond([], "", 204);
                } else {
                    respond([], "Unable to choose the store", 500);
                }
                
                break;

                
            case "new_delivery_address":
                // Need to use suffixes
                $oc_customer_id = checkSession("oc_customer_id");
                $oc_first_name = checkSession("oc_first_name");
                $oc_last_name = checkSession("oc_last_name");

                $params = ["street" => true, "number" => true, "complement" => false, "zipcode" => true, "district" => true, "city" => true, "uf" => true];
                $data = [];
                foreach ($params as $param => $mandatory) {
                    $data[$param] = filter_input(INPUT_POST, $param);
                    if (!isset($param)) {
                        respond([], "Parameter '$param' not found", 401);
                    }
                    if ($mandatory && (strlen($data[$param]) == 0)) {
                        respond([], "Field '$param' cannot be blank", 401);
                    }
                }

                $customer = new Customer($suffix_app, $suffix_oc);
                if ($customer->newAddress($oc_customer_id, $oc_first_name, $oc_last_name, $data)) {
                    respond([], "", 204);
                } else {
                    respond([], "Unable add the new address", 500);
                }

                break;

            case "choose_delivery_address":
                // Need to use suffixes
                $oc_customer_id = checkSession("oc_customer_id");
                $oc_customer_group_id = checkSession("oc_customer_group_id");

                $id = filter_input(INPUT_POST, "id");
                if (!isset($id)) {
                    respond([], "Parameter Id not found", 401);
                }

                $customer = new Customer($suffix_app, $suffix_oc);
                $response = $customer->chooseAddress($oc_customer_id, $oc_customer_group_id, $id);

                if ($response === false) { 
                    respond([], "", 500);
                } else { 
                    respond([], $response, 200);
                }

                break;

            default:
                respond([], "Method not allowed", 405);
                break;
        }
    } else {
        respond([], "Method not found", 401);
    }

    function respond($data = [], $message = "", $http_code = 200) {
        // OK with no content
        if ($http_code == 204) {
            http_response_code(204);
            die();
        }
        
        $answer = [];
        $answer["message"] = $message; 
        $answer["data"] = $data;
        http_response_code($http_code);
        die(json_encode($answer));
    }

    function checkSession($suffix) {
        $answer = $_SESSION[$suffix];
        if (!isset($answer)) { 
            respond([], "Session data $suffix not found", 500);
        }
        return $answer;
    }

    function getDOWname($dow) {
        $names = ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"];
        if (!isset($dow)) return false;
        if (!is_numeric($dow)) return false;
        if ($dow < 0) return false;
        if ($dow > 6) return false;
        return $names[$dow];
    }
?>
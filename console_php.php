<?php

// Debuger 
function console_php( $data ) {
	ob_start();
	$output  = "<script>console.log( 'PHP debugger: ";
	$output .= json_encode( print_r( $data, true ) );
	$output .= "' );</script>";
	echo $output;
}

?>